package user

import (
    "database/sql"
    
	"gitlab.com/gmochid/azura-udemy/db"
)

type User struct {
    ID       int
    Username string
    Password string
}

func FindByUsername(username string) (*User, error) {
    row := db.DB.QueryRow("SELECT id, username, password FROM users WHERE username = $1", username)

    var user User
    err := row.Scan(&user.ID, &user.Username, &user.Password)
    if err != nil {
        if err == sql.ErrNoRows {
            return nil, nil
        }
        return nil, err
    }

    return &user, nil
}