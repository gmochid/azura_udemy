package main

import (
	"gitlab.com/gmochid/azura-udemy/middleware"
	"gitlab.com/gmochid/azura-udemy/db"
    "github.com/gofiber/fiber/v2"
)

func main() {
    db.Connect()

    app := fiber.New()
    app.Use(middleware.BasicAuth())
    app.Get("/", func(c *fiber.Ctx) error {
        err := c.JSON(fiber.Map{
            "status": "ok",
        })
        return err
    })

    // Listen on PORT 3000
    app.Listen(":3000")
}
