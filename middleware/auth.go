package middleware

import (
    "encoding/base64"
    "github.com/gofiber/fiber/v2"
    
	"gitlab.com/gmochid/azura-udemy/service"
    "strings"
)

func BasicAuth() fiber.Handler {
    return func(c *fiber.Ctx) error {
        authHeader := c.Get("Authorization")
        if authHeader == "" {
            return c.Status(fiber.StatusUnauthorized).SendString("Unauthorized")
        }

        auth := strings.SplitN(authHeader, " ", 2)
        if len(auth) != 2 || auth[0] != "Basic" {
            return c.Status(fiber.StatusUnauthorized).SendString("Unauthorized")
        }

        payload, err := base64.StdEncoding.DecodeString(auth[1])
        if err != nil {
            return c.Status(fiber.StatusUnauthorized).SendString("Unauthorized")
        }

        pair := strings.SplitN(string(payload), ":", 2)
        if len(pair) != 2 {
            return c.Status(fiber.StatusUnauthorized).SendString("Unauthorized")
        }

        username := pair[0]
        password := pair[1]

        user, err := user.FindByUsername(username)
        if err != nil {
            return c.Status(fiber.StatusInternalServerError).SendString("Internal Server Error")
        }

        if user == nil {
            return c.Status(fiber.StatusUnauthorized).SendString("Unauthorized")
        }

        if user.Password != password {
            return c.Status(fiber.StatusUnauthorized).SendString("Unauthorized")
        }

        return c.Next()
    }
}