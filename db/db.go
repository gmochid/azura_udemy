package db

import (
    "database/sql"
    "fmt"
    "os"
    
    "github.com/joho/godotenv"
    _ "github.com/lib/pq"
)

var DB *sql.DB

func Connect() {
    err := godotenv.Load()
    if err != nil {
        fmt.Println("Error loading .env file")
        return
    }

    dbString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", 
        os.Getenv("DB_HOST"), 
        os.Getenv("DB_PORT"), 
        os.Getenv("DB_USER"), 
        os.Getenv("DB_PASSWORD"), 
        os.Getenv("DB_NAME"))
    db, err := sql.Open("postgres", dbString)
    if err != nil {
        panic(err)
    }

    DB = db
}
